Task
----

The team wants to start working on Happs which is a small project that uses PHP 7.1 and Mariadb 10.2.8.
Please create a docker image/s following proper standards, which would allow our developers to quickly start
working on this project. The same image/s will be used in production & Jenkins so that developers have the
same environment as production. This will ensure we do not encounter any environmental issues in the
future. Our developers are afraid that each time they perform a code change locally they must wait for a long
time to see their changes when testing on their local docker container. We are keen on finding a solution to
the described scenario.


#### Requirements

- Docker image/s (file/s) for Happs
- Readme file describing your work and any decisions you had to take
- Fast Deployment
- Make sure developers can easily change the code deployed in their local docker container


Implementation
--------------

The described project will be implemented as a three-tier application stack consisting of:

- Nginx web server
- PHP-FPM engine 
- Mariadb database

The application components and interfaces will be defined and created using *docker-compose* which will enable developers to quickly start working on the project locally. The only requirement on the development machine is to have *docker* client and *docker-compose* installed. 

Having the application stack defined in a docker-compose configuration shared between dev/test/prod environments will prevent issues when running the stack on different environments.

The skeleton of the stack is the `docker-compose.yml` file which describes the services/components of the stack, their dependencies and data volumes shared between different components. 

The environment specifics are described in the `docker-compose.(dev|test|prod).yml`. By combining these files with the skeleton `docker-compose.yml` (using multiple `docker-compose -f` options) the stack can be adjusted to each environment's requirements whilst maintaining the general application architecture across environments. For instance, the `docker-compose.dev.yml` creates a bind-mount on the application code and web server configuration to enable developers to quickly make changes in their local environment. Additional environment specific configuration in these files are database passwords and Laravel-specific configuration defined in the `.env.*` files. Also, the `docker-compose.dev.yml` adds additional service to help developers with DB administration. 


#### Scenarios

##### Development

1. Clone GIT repo

2. Install required packages using composer

    ```
    $ ./composer_update.sh
    ```

3. Predefine *docker-compose* parameters

    ```
    $ export COMPOSE_FILE="docker-compose.yml:docker-compose.dev.yml"
    ```

3. Run local stack

    ```
    $ docker-compose build
    $ docker-compose up -d
    ```

4. Initialize database 

    ```
    $ ./init_db.sh
    ```

The application will be available on `http://<docker_host>:80` and the DB admin tool on `http://<docker_host>:8080`. Change to the application code will take effect immediately due to the bind-mount. 

To reload changes to the nginx configuration restart the nginx container: `docker-compose restart web`.


#### CI and QA

The included `Jenkinsfile` demonstrates a simple CI configuration and the usage of the CI-specific `docker-compose.ci.yml` configuration. Each successfull build tags the created docker images with the branch name and build number and pushes them to the registry. These images can later be used to quickly deploy the application stack. 


#### Deployment

The production-specific `docker-compose.prod.yml` configuration can be used to quickly deploy a specific version of the application stack from the images pushed to the registry at the end of a successful CI build. For the sake of simplicity, this is a regular *docker-compose* configuration file, but can easily be adjusted for docker swarm or the images can be deployed to a more advanced orchestration platform. 

Supposing the production environment is managed using *docker-machine*:

1. Predefine *docker-compose* parameters

    ```
    $ export COMPOSE_FILE="docker-compose.yml:docker-compose.prod.yml"
    ```
2. Point docker client to the production docker engine:

    ```
    $ eval $(docker-machine env prod)
    ```

3. Run production stack

    ```
    $ VERSION=1.2 docker-compose up -d
    ```

Deploying to production presumes that there is already an existing database so the `docker-compose.prod.yml` references and external persistent data volume.

#### Points of improvement

This project provides a simple solution to the task which in a real-world scenario should take into consideration a more secure credentials management, network security and a more sofisticated branch and version management.