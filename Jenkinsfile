pipeline {

    agent any

    environment {
        COMPOSE_PROJECT_NAME = "${env.JOB_BASE_NAME}${env.GIT_BRANCH}${env.BUILD_NUMBER}"
        COMPOSE_FILE = "docker-compose.yml:docker-compose.ci.yml"
    }

    stages {

        stage("Prepare") {
            steps {
                sh "echo Installing app dependencies..."
                sh "./composer_update.sh"
            }
        }

        stage("Run static tests") {
            steps {
                sh "Running unit/code quality tests here..."
            }
        }

        stage("Build images") {
            steps {
                sh "echo Building images..."
                sh "docker-compose build"
            }
        }

        stage("Start test stack") {
            steps {
                sh "echo Starting services..."
                sh "docker-compose up -d"

                //Create app url from docker host address and the dynamic port of the nginx container to use in dynamic tests
                sh "echo Getting app URL..."
                script {
                    env.DOCKER_HOST = sh (returnStdout: true, script: "docker-machine ip").trim()
                    env.APP_PORT = sh (returnStdout: true, script: "docker-compose port web 80 | awk -F':' '{print \$2}'").trim()
                    env.APP_URL = "http://${env.DOCKER_HOST}:${env.APP_PORT}"
                }

                //Wait for all services to come online
                //sleep() or user something more intelligent like softonic/compose-project-is-up
            }
        }

        stage("Init DB") {
            steps {
                sh "echo Initializing database..."
                sh "./init_db.sh"
            }
        }

        stage("Run dynamic tests") {
            steps {
                sh "echo Running integration/UI tests here..."
            }
        }
    }

    post {
      success {
            //Tag the created images and push them to the registry
            sh "docker image tag ${env.COMPOSE_PROJECT_NAME}_app:latest ${env.JOB_BASE_NAME}/app:${env.GIT_BRANCH}-${env.BUILD_NUMBER}"
            sh "docker image push ${env.JOB_BASE_NAME}/app:${env.GIT_BRANCH}-${env.BUILD_NUMBER}"

            sh "docker image tag ${env_COMPOSE_PROJECT_NAME}_web:latest ${env.JOB_BASE_NAME}/web:${env.GIT_BRANCH}-${env.BUILD_NUMBER}"
            sh "docker image push ${env.JOB_BASE_NAME}/web:${env.GIT_BRANCH}-${env.BUILD_NUMBER}"

            sh "echo Success!"
      }

      failure {
            sh "echo Something went wrong :("
      }

      always {
            //Archive test reports and other artifacs
            archive 'reports/**/*.html'

            //Clean up stack
            sh "docker-compose down --rmi=local -v --remove-orphans"
      }
    }
}